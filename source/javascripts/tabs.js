/* global jQuery */
(function($) {
  'use strict';

  var hash = document.location.hash;
  if (hash) {
    if (hash.substr(0, 1) === '#') {
      hash = hash.substr(1);
    }
    $(function() {
      var $tabLink = $('a[href="#' + hash + '"]');
      var $tabContainer = $tabLink.closest('[role="tablist"]');
      if ($tabLink.length === 1) {
        $tabLink.tab('show');

        if ($tabContainer.length === 1) {
          window.setTimeout(function() {
            window.scrollTo(0, $tabContainer.scrollTop());
          }, 500);
        }
      }
    });
  }
}(jQuery));
