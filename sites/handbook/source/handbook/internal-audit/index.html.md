---
layout: handbook-page-toc
title: "The Internal Audit Function"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# The Mission of Internal Audit

To enhance and protect organizational value by providing risk-based and objective assurance, advice and insight, while consistently building trust and strengthening the relationship with management, through the delivery of high quality and distinctive internal audit services.

Internal Audit is an independent, objective assurance and consulting activity designed to add value and improve organization’s operations. 

Internal Audit is responsible to assess the effectiveness of risk management, control and governance processes and to provide insight and recommendations that can enhance these processes, particularly relating to: 

* Effectiveness of operations;
* Reliability of financial management and reporting; and
* Compliance with laws and regulations.

Internal Audit may also involve conducting fraud investigations to identify fraudulent acts and conducting post investigation fraud audits to identify control breakdowns and establish financial loss.

## How Internal Audit Adds Value

* Internal Audit works closely with management to review systems and operations to identify how well risks are managed, whether the right processes are in place, and whether agreed procedures are being followed. This provides an indication of the integrity of the organization’s systems and processes, their capability to support the set goals and also helps identify areas for improvements. 

* Internal Audit works across all areas of an organization, review tangible (e.g. supply chain/ IT systems) and intangible (e.g. organization culture and ethics) aspects of operations. 

* Internal Audit looks beyond financial statements and financial risks, and consider wider issues, e.g. the organization’s reputation, growth, impact on the environment, and how employees are treated. Any process that has an impact on the effective operation of an organization may be included in internal audit’s scope.

Internal Audit reports administratively to the Principal Accounting Officer but has direct responsibility and reporting to the Board of Directors through an Audit Committee. 



# Contact the Internal Audit Team

* Email
   * `internal-audit@gitlab.com`

* Tag us in GitLab
   * `@gitlab-com/internal-audit`

* Slack
   * Feel free to use tag `@int-audit`
   * The `#internal_audit` slack channel is the best place for questions relating to our team (please add the above tag)

* [Sarbanes-Oxley (SOX) Compliance](https://about.gitlab.com/handbook/internal-audit/sarbanes-oxley/)

* Interested in joining our team? Check out more [here](https://about.gitlab.com/job-families/finance/internal-audit/)
 



  



